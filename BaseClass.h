#pragma once

#include <stdlib.h>

// BaseClass data structure
typedef struct BaseClass
{
    int a;
    int b;
    
    // Pointer to implementation of virtual function
    void(*sayHelloVirtualImplementation)(struct BaseClass* self);

}BaseClass;

// BaseClass method pointers
typedef struct
{
    BaseClass* (*create)();
    int        (*sum)(BaseClass* self);
    void       (*setAB)(BaseClass* self, int a, int b);
    
    // "Standard" interface for calling virtual function
    void       (*sayHelloVirtual)(BaseClass* self);
}__BaseClass;

extern __BaseClass _BaseClass;