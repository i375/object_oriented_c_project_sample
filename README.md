#Object oriented C project sample#

Convention based OOP design.

See comment for conventions.

**Files of interest:**

[BaseClass.h](BaseClass.h)

[BaseClass.c](BaseClass.c)

[ChildClass.h](ChildClass.h)

[ChildClass.c](ChildClass.c)

[main.c](main.c)