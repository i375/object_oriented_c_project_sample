#include "ChildClass.h"
#include <stdio.h>

static void sayHelloVirtualImplementation(BaseClass* self)
{
    printf("Hello from child class\n");
}

static ChildClass* create()
{
    ChildClass* self = (ChildClass*)malloc(sizeof(ChildClass));

    // Overriding sayHelloVirtualImplementation
    ((BaseClass*)self)->sayHelloVirtualImplementation = sayHelloVirtualImplementation;

    return self;
}

static void setC(ChildClass *self, int c)
{
    self->c = c;
}

// Setting public method pointers
__ChildClass _ChildClass = {
    .create = create,
    .setC = setC
};

