#pragma once

#include "BaseClass.h"

// Child class data structure
typedef struct
{
    // Extending BaseClass structure
    // Always first element of struct
    //
    // This is required for ((BaseClass*)childClass)
    // down cast to work.
    BaseClass super;
    
    // Adding fields
    int c;
}ChildClass;

// Child class method pointers
typedef struct
{
    ChildClass* (*create)();
    void(*setC)(ChildClass *self, int c);
}__ChildClass;

extern __ChildClass _ChildClass;