#include <stdio.h>
#include "ChildClass.h"

int main()
{
    BaseClass *bc = _BaseClass.create();
    ChildClass *cc = _ChildClass.create();

    // Calling base class method
    _BaseClass.setAB(bc, 1, 2);
    int baseSum = _BaseClass.sum(bc);

    // Calling base class methods on child class
    // Here you can check weather downcast is valid
    // following "super" fields
    _BaseClass.setAB((BaseClass*)cc, 10, 15);
    int childSum = _BaseClass.sum((BaseClass*)cc);

    _ChildClass.setC(cc, 5);

    // Virtual functions

    // Calls BaseClass implementation of virtual function
    _BaseClass.sayHelloVirtual(bc);

    // Calls ChildClass implementation of virtual function
    _BaseClass.sayHelloVirtual((BaseClass*)cc);

    return 0;
}