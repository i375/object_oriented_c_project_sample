#pragma once

#include <stdio.h>
#include "BaseClass.h"

// Actual implementation of sayHelloVirtual
static void sayHelloVirtualImplementation(BaseClass* self)
{
    printf("Hello from base class\n");
}

static void sayHelloVirtual(BaseClass* self)
{
    self->sayHelloVirtualImplementation(self);
}

static BaseClass* create()
{
    BaseClass *mc = (BaseClass*)malloc(sizeof(BaseClass));

    mc->sayHelloVirtualImplementation = sayHelloVirtualImplementation;

    return mc;
}

static int sum(BaseClass* self)
{
    return self->a + self->b;
}

static void setAB(BaseClass* self, int a, int b)
{
    self->a = a;
    self->b = b;
}

// Setting public method pointers
__BaseClass _BaseClass = {
    .create = create,
    .sum = sum,
    .setAB = setAB,
    .sayHelloVirtual = sayHelloVirtual
};